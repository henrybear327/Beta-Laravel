@extends('app')

@section('title')
Ecourse
@stop

@section('content')

    <div class="jumbotron">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <p class="text-center">
                    @if (true)
                        Easy php with Laravel! Hi Bemg!
                    @else
                        Not easy php with Laravel!
                    @endif
                </p>

                @if (count($people))
                <ul>
                    @foreach($people as $person)
                        <li>{{ $person }}</li>
                    @endforeach
                </ul>
                @endif
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>

    @stop
