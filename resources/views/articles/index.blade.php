@extends('app')

@section('title')
    Articles
@stop

@section('content')

    <div class="jumbotron">
        <h1>Articles!</h1>

        <a href="{{ url('articles/create') }}">Create!</a>

        <div class="row">
            <div class="col-md-6">
                @foreach($articles as $article)
                    <article>
                        <h2>Title: {{ $article->title }}</h2>

                        <p>Body: {{ $article->body }}</p>
                        <p>Published time: {{ $article->published_at }}</p>
                    </article>
                @endforeach
            </div>

            <div class="col-md-6">
                @foreach($articles as $article)
                    <article>
                        <h2>
                            Title:<br>
                            <a href="/articles/{{ $article->id }}">{{ $article->title }}</a>
                            <a href="{{ action('ArticlesController@show', [$article->id]) }}">{{ $article->title }}</a>
                            <a href="{{ url('/articles', $article->id) }}">{{ $article->title }}</a>
                        </h2>

                        <p>Body: {{ $article->body }}</p>
                        <p>Published time: {{ $article->published_at }}</p>
                    </article>
                @endforeach
            </div>
        </div>
    </div>

@stop
