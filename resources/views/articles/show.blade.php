@extends('app')

@section('title')
    Articles ID
@stop

@section('content')

    <div class="jumbotron">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <h1>Title: {{ $article->title }}</h1>

                <p>Body: {{ $article->body}}</p>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>

@stop
