<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('datepicker', 'WelcomeController@datepicker');
Route::post('datepicker', 'WelcomeController@show');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'WelcomeController@index'); // double press shift to use quick jump
    Route::get('/contact', 'WelcomeController@contact');

    Route::get('about', 'PagesController@about');

    Route::get('foo', function() {
        return 'foo';
    });

    Route::get('articles', 'ArticlesController@index');
    Route::get('articles/create', 'ArticlesController@create'); // mind the common pitfall, this MUST come before articles/{id}
    Route::post('articles/create', 'ArticlesController@store');
    Route::get('articles/{id}', 'ArticlesController@show');

    Route::get('app_old', 'WelcomeController@app_old');


});
