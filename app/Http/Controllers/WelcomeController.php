<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class WelcomeController extends Controller
{
    public function index() {
        $people = [
            'Hello', "There", "!"
        ];
        return view('welcome', compact('people'));
        // return 'hello world!';
    }

    public function contact() {
        return view('pages.contact');
    }

    public function app_old() {
        return view('app_old');
    }

    public function datepicker() {
        return view('datepicker');
    }

    public function show(Request $request) {
        dd($request);
    }

}
