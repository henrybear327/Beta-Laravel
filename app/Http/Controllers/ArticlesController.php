<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use Carbon\Carbon;
use Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Article as Article;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $articles = Article::latest('published_at')->where('published_at', '<=', Carbon::now())->get(); // don't show future post!
        // $articles = Article::latest('published_at')->unpublished()->get();
        $articles = Article::latest('published_at')->published()->get();
        // articles = Article::latest('published_at')->get();

        // dd($articles);

        // return $articles; // returns json
        return view('articles.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(/* Request $request */)
    {
        /* For regular POST request
        // dd($request);

        // dump($request->input('email'));
        // dump($request->input('password'));
        */

        Article::create(Request::all()); // the fillable field must be set for this kind of mass assignment!

        return redirect('articles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*
        $article = Article::find($id); // null if not found

        // dd($article);

        // check for null (bad way)
        if(is_null($article)) {  // if(!$article)
            abort(404);
        }
        */

        $article = Article::findOrFail($id); // null if not found

        return view('articles.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}