<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    // may be mass assigned
    protected $fillable = [
        'title', 'body', 'published_at'
    ];

    protected $dates = ['published_at'];

    // set published at attribute
    // Watch out for the naming convention
    public function setPublishAtAttribute($date) {
        $this->attributes['published_at'] = Carbon::createFromFormat('Y-m-d', $date);
        // $this->attributes['published_at'] = Carbon::parse($date);
    }

    public function scopePublished($query) {
        $query->where('published_at', '<=', Carbon::now());
    }

    public function scopeunPublished($query) {
        $query->where('published_at', '>', Carbon::now());
    }
}